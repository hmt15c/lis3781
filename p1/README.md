> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781

## Michael Tranor

### Project #1 Requirements:

*Sub-Heading:*

1. Create ERD
2. Create tables from SQL code
3. Populate tables
4. Screenshots of Code and Results

#### README.md file should include the following items:

* SQL Code Screenshots
* Populated Tables Screenshots 
* ERD Screenshot

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots:

*Screenshot of ERD*:

![Screenshot of ERD](img/lis3781_p1_ERD.png)

*Screenshot of Populated Tables*:

![Screenshot of Populated Tables](img/lis3781_p1_populated_1.png)

*Screenshot of Populated Tables*:

![Screenshot of Populated Tables](img/lis3781_p1_populated_2.png)

*Screenshot of Populated Tables*:

![Screenshot of Populated Tables](img/lis3781_p1_populated_3.png)

*Screenshot of Populated Tables*:

![Screenshot of Populated Tables](img/lis3781_p1_populated_4.png)

*Screenshot of SQL Code*:

![Screenshot of SQL Code](img/lis3781_p1_sql_1.png)

*Screenshot of SQL Code*:

![Screenshot of SQL Code](img/lis3781_p1_sql_2.png)

*Screenshot of SQL Code*:

![Screenshot of SQL Code](img/lis3781_p1_sql_3.png)

*Screenshot of SQL Code*:

![Screenshot of SQL Code](img/lis3781_p1_sql_4.png)

*Screenshot of SQL Code*:

![Screenshot of SQL Code](img/lis3781_p1_sql_5.png)

*Screenshot of SQL Code*:

![Screenshot of SQL Code](img/lis3781_p1_sql_6.png)

*Screenshot of SQL Code*

![Screenshot of SQL Code](img/lis3781_p1_sql_7.png)

*Screenshot of SQL Code*:

![Screenshot of SQL Code](img/lis3781_p1_sql_8.png)

*Screenshot of SQL Code*:

![Screenshot of SQL Code](img/lis3781_p1_sql_9.png)

*Screenshot of SQL Code*:

![Screenshot of SQL Code](img/lis3781_p1_sql_10.png)

*Screenshot of SQL Code*:

![Screenshot of SQL Code](img/lis3781_p1_sql_11.png)

*Screenshot of SQL Code*:

![Screenshot of SQL Code](img/lis3781_p1_sql_12.png)

*Screenshot of SQL Code*:

![Screenshot of SQL Code](img/lis3781_p1_sql_13.png)

*Screenshot of SQL Code*:

![Screenshot of SQL Code](img/lis3781_p1_sql_14.png)

*Screenshot of SQL Code*:

![Screenshot of SQL Code](img/lis3781_p1_sql_15.png)

*Screenshot of SQL Code*:

![Screenshot of SQL Code](img/lis3781_p1_sql_16.png)



#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
