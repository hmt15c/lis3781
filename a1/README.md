> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781

## Michael Tranor

### Assignment #1 Requiremnts:



1. Distributed Version Control with Git and Bitbucket
2. AMPPS Installation
3. Questions
4. Entity Relationship Diagram and SQL Code (optional)
5. Bitbucket repo links

#### README.md file should include the following items:

* Screenshot of A1 ERD
* git commands w/ short descriptions
* 


>
> #### Git commands w/short descriptions:

1. git init - creates a new repository
2. git status - displays state of working directory and staging area
3. git add - moves changes from the working directory to git staging area
4. git commit - takes staged snapshot and commits to project history 
5. git push - upload local repository content to remote repository 
6. git pull - fetch and download content from remote repository and update local repository to match
7. git branch - list branches

#### Assignment Screenshots:

*Screenshot of A1 ERD*:

![Screenshot of A1 ERD](img/lis3781_a1_ERD.png)




#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
