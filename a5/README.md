> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781

## Michael Tranor

### Assignment #5 Requirements:

*Sub-Heading:*

1. Use Microsoft SQL Server Remote Desktop
2. Populate Tables using T- SQL
3. Create Entity Relationship Diagram (ERD)
4. Use A4 T-SQL, add additional tables including Time table
5. Insert additional data for person table and new tables

#### README.md file should include the following items:



#### Assignment Screenshots:

*Screenshot of Populated Tables *:

![Screenshot Populated Tables](img/LIS3781_A5_populated_1.png)

*Screenshot of Populated Tables *:

![Screenshot Populated Tables](img/LIS3781_A5_populated_2.png)

*Screenshot of Populated Tables *:

![Screenshot Populated Tables](img/LIS3781_A5_populated_3.png)

*Screenshot of Populated Tables *:

![Screenshot Populated Tables](img/LIS3781_A5_populated_4.png)

*Screenshot of Populated Tables *:

![Screenshot Populated Tables](img/LIS3781_A5_populated_5.png)

*Screenshot of SQL Code *:

![Screenshot SQL Code](img/LIS3781_A5_sql_1.png)

*Screenshot of SQL Code *:

![Screenshot SQL Code](img/LIS3781_A5_sql_2.png)

*Screenshot of SQL Code *:

![Screenshot SQL Code](img/LIS3781_A5_sql_3.png)

*Screenshot of SQL Code *:

![Screenshot SQL Code](img/LIS3781_A5_sql_4.png)

*Screenshot of SQL Code *:

![Screenshot SQL Code](img/LIS3781_A5_sql_5.png)

*Screenshot of SQL Code *:

![Screenshot SQL Code](img/LIS3781_A5_sql_6.png)

*Screenshot of SQL Code *:

![Screenshot SQL Code](img/LIS3781_A5_sql_7.png)

*Screenshot of SQL Code *:

![Screenshot SQL Code](img/LIS3781_A5_sql_8.png)

*Screenshot of SQL Code *:

![Screenshot SQL Code](img/LIS3781_A5_sql_9.png)

*Screenshot of SQL Code *:

![Screenshot SQL Code](img/LIS3781_A5_sql_10.png)

*Screenshot of SQL Code *:

![Screenshot SQL Code](img/LIS3781_A5_sql_11.png)

*Screenshot of SQL Code *:

![Screenshot SQL Code](img/LIS3781_A5_sql_12.png)

*Screenshot of SQL Code *:

![Screenshot SQL Code](img/LIS3781_A5_sql_13.png)

*Screenshot of SQL Code *:

![Screenshot SQL Code](img/LIS3781_A5_sql_14.png)

*Screenshot of SQL Code *:

![Screenshot SQL Code](img/LIS3781_A5_sql_15.png)

*Screenshot of SQL Code *:

![Screenshot SQL Code](img/LIS3781_A5_sql_16.png)

*Screenshot of SQL Code *:

![Screenshot SQL Code](img/LIS3781_A5_sql_17.png)

*Screenshot of SQL Code *:

![Screenshot SQL Code](img/LIS3781_A5_sql_18.png)

*Screenshot of SQL Code *:

![Screenshot SQL Code](img/LIS3781_A5_sql_19.png)

*Screenshot of SQL Code *:

![Screenshot SQL Code](img/LIS3781_A5_sql_20.png)

*Screenshot of SQL Code *:

![Screenshot SQL Code](img/LIS3781_A5_sql_21.png)

*Screenshot of SQL Code *:

![Screenshot SQL Code](img/LIS3781_A5_sql_22.png)

*Screenshot of SQL Code *:

![Screenshot SQL Code](img/LIS3781_A5_sql_23.png)

*Screenshot of SQL Code *:

![Screenshot SQL Code](img/LIS3781_A5_sql_24.png)

*Screenshot of SQL Code *:

![Screenshot SQL Code](img/LIS3781_A5_sql_25.png)

*Screenshot of Table ERD *:

![Screenshot of ERD](img/LIS3781_ERD_A5.png)








#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
