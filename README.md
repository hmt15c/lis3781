> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS3781

## Dr. Mark K. Jowett

### Class Number Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install Ammps
    - Create ERD and Database
	- Create database for HR department

2. [A2 README.md](a2/README.md "My A2 README.md file")
	- Create Bitbucket repo
	- complete Bitbucket tutorial
	- Git command descriptions
	- Screenshots of SQL code and query results

3. [A3 README.md](a3/README.md "My A3 README.md file")
	- Install Oracle 
	- Populate Tables
	- Screenshot of sql code and query results 
4. [P1 README.md](p1/README.md "My P1 README.md file")
	- Create ERD
	- Create Database for local municipality 
	- Screenshots of Populated Tables and SQL Code
5. [A4 README.md](a4/README.md "My A4 README.md file")
	- Use Microsoft SQL Server Remote Desktop
	- Populate Tables using T-SQL
	- Create Entity Relationship Diagram (ERD)
	- Create Stored Procedure to add to Person table and salt and hash
6. [A5 README.md](a5/README.md "My A5 README.md file")
	- Use Microsoft SQL Server Remote Desktop
	- Populate Tables using T- SQL
	- Create Entity Relationship Diagram (ERD)
	- Use A4 T-SQL, add additional tables including Time table
	- Insert additional data for person table and new tables

