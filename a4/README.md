> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781

## Michael Tranor

### Assignment #4 Requirements:

*Sub-Heading:*

1. Use Microsoft SQL Server Remote Desktop
2. Populate Tables using T-SQL
3. Create Entity Relationship Diagram (ERD)
4. Create Stored Procedure to add to Person table and salt and hash

#### README.md file should include the following items:



#### Assignment Screenshots:

*Screenshot of SQL Code *:

![Screenshot SQL Code](img/LIS3781_A4_sql_1.png)

*Screenshot of SQL Code *:

![Screenshot SQL Code](img/LIS3781_A4_sql_2.png)

*Screenshot of SQL Code *:

![Screenshot SQL Code](img/LIS3781_A4_sql_3.png)

*Screenshot of SQL Code *:

![Screenshot SQL Code](img/LIS3781_A4_sql_4.png)

*Screenshot of SQL Code *:

![Screenshot SQL Code](img/LIS3781_A4_sql_5.png)

*Screenshot of SQL Code *:

![Screenshot SQL Code](img/LIS3781_A4_sql_6.png)

*Screenshot of SQL Code *:

![Screenshot SQL Code](img/LIS3781_A4_sql_7.png)

*Screenshot of SQL Code *:

![Screenshot SQL Code](img/LIS3781_A4_sql_8.png)

*Screenshot of SQL Code *:

![Screenshot SQL Code](img/LIS3781_A4_sql_9.png)

*Screenshot of SQL Code *:

![Screenshot SQL Code](img/LIS3781_A4_sql_10.png)

*Screenshot of SQL Code *:

![Screenshot SQL Code](img/LIS3781_A4_sql_11.png)

*Screenshot of SQL Code *:

![Screenshot SQL Code](img/LIS3781_A4_sql_12.png)

*Screenshot of SQL Code *:

![Screenshot SQL Code](img/LIS3781_A4_sql_13.png)

*Screenshot of SQL Code *:

![Screenshot SQL Code](img/LIS3781_A4_sql_14.png)

*Screenshot of Populated Tables *:

![Screenshot SQL Code](img/LIS3781_A4_populated_1.png)

*Screenshot of Populated Tables *:

![Screenshot SQL Code](img/LIS3781_A4_populated_2.png)

*Screenshot of Populated Tables *:

![Screenshot SQL Code](img/LIS3781_A4_populated_3.png)

*Screenshot of Populated Tables *:

![Screenshot SQL Code](img/LIS3781_A4_populated_4.png)

*Screenshot of Stored Procedure to add to People table, then salt and hash *:

![Screenshot SQL Code](img/LIS3781_A4_storedprocedure.png)

*Screenshot of ERD tables *:

![Screenshot SQL Code](img/LIS3781_ERD_A4.png)




#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
