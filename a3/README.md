> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781

## Michael Tranor

### Assignment #3 Requirements:

*Sub-Heading:*

1. Install Oracle 
2. Populate Tables
3. Screenshot of sql code and query results 

#### README.md file should include the following items:

> #### Git commands w/short descriptions:

1. git init - creates a new repository
2. git status - displays state of working directory and staging area
3. git add - moves changes from the working directory to git staging area
4. git commit - takes staged snapshot and commits to project history 
5. git push - upload local repository content to remote repository 
6. git pull - fetch and download content from remote repository and update local repository to match
7. git branch - list branches 

#### Assignment Screenshots:

*Screenshot of SQL Code *:

![Screenshot SQL Code](img/lis3781_a3_code_a.png)

*Screenshot of SQL Code*:

![Screenshot SQL Code](img/lis3781_a3_code_b.png)

*Screenshot of SQL Code*:

![Screenshot SQL Code](img/lis3781_a3_code_c.png)

*Screenshot of Populated Tables in Oracle*:

![Screenshot of Populated Tables in Oracle](img/lis3781_a3_tables.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
